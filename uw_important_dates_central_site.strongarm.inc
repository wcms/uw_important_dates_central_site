<?php

/**
 * @file
 * uw_important_dates_central_site.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function uw_important_dates_central_site_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'clone_reset_uw_ct_important_dates_central';
  $strongarm->value = 1;
  $export['clone_reset_uw_ct_important_dates_central'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_anonymous_uw_ct_important_dates_central';
  $strongarm->value = '0';
  $export['comment_anonymous_uw_ct_important_dates_central'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_default_mode_uw_ct_important_dates_central';
  $strongarm->value = 1;
  $export['comment_default_mode_uw_ct_important_dates_central'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_default_per_page_uw_ct_important_dates_central';
  $strongarm->value = '50';
  $export['comment_default_per_page_uw_ct_important_dates_central'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_form_location_uw_ct_important_dates_central';
  $strongarm->value = 1;
  $export['comment_form_location_uw_ct_important_dates_central'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_preview_uw_ct_important_dates_central';
  $strongarm->value = '1';
  $export['comment_preview_uw_ct_important_dates_central'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_subject_field_uw_ct_important_dates_central';
  $strongarm->value = 1;
  $export['comment_subject_field_uw_ct_important_dates_central'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_uw_ct_important_dates_central';
  $strongarm->value = '1';
  $export['comment_uw_ct_important_dates_central'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'date_format_uw_date_mportant_dates';
  $strongarm->value = 'M j, Y';
  $export['date_format_uw_date_mportant_dates'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'diff_enable_revisions_page_node_uw_ct_important_dates_central';
  $strongarm->value = 1;
  $export['diff_enable_revisions_page_node_uw_ct_important_dates_central'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'diff_show_preview_changes_node_uw_ct_important_dates_central';
  $strongarm->value = 1;
  $export['diff_show_preview_changes_node_uw_ct_important_dates_central'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'diff_view_mode_preview_node_uw_ct_important_dates_central';
  $strongarm->value = 'full';
  $export['diff_view_mode_preview_node_uw_ct_important_dates_central'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'field_bundle_settings_node__uw_ct_important_dates_central';
  $strongarm->value = array(
    'view_modes' => array(
      'teaser' => array(
        'custom_settings' => TRUE,
      ),
      'entity_teaser' => array(
        'custom_settings' => TRUE,
      ),
      'embedded' => array(
        'custom_settings' => TRUE,
      ),
      'full' => array(
        'custom_settings' => FALSE,
      ),
      'rss' => array(
        'custom_settings' => FALSE,
      ),
      'search_index' => array(
        'custom_settings' => FALSE,
      ),
      'search_result' => array(
        'custom_settings' => FALSE,
      ),
      'ical' => array(
        'custom_settings' => FALSE,
      ),
      'diff_standard' => array(
        'custom_settings' => FALSE,
      ),
      'token' => array(
        'custom_settings' => FALSE,
      ),
    ),
    'extra_fields' => array(
      'form' => array(
        'locations' => array(
          'weight' => '4',
        ),
        'metatags' => array(
          'weight' => '5',
        ),
        'title' => array(
          'weight' => '0',
        ),
        'path' => array(
          'weight' => '1',
        ),
        'redirect' => array(
          'weight' => '3',
        ),
        'xmlsitemap' => array(
          'weight' => '2',
        ),
      ),
      'display' => array(),
    ),
  );
  $export['field_bundle_settings_node__uw_ct_important_dates_central'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'language_content_type_uw_ct_important_dates_central';
  $strongarm->value = '0';
  $export['language_content_type_uw_ct_important_dates_central'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_options_uw_ct_important_dates_central';
  $strongarm->value = array(
    0 => 'main-menu',
  );
  $export['menu_options_uw_ct_important_dates_central'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_parent_uw_ct_important_dates_central';
  $strongarm->value = 'main-menu:0';
  $export['menu_parent_uw_ct_important_dates_central'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_options_uw_ct_important_dates_central';
  $strongarm->value = array(
    0 => 'moderation',
    1 => 'revision',
  );
  $export['node_options_uw_ct_important_dates_central'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_preview_uw_ct_important_dates_central';
  $strongarm->value = '1';
  $export['node_preview_uw_ct_important_dates_central'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_submitted_uw_ct_important_dates_central';
  $strongarm->value = 0;
  $export['node_submitted_uw_ct_important_dates_central'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'page_load_progress_elements';
  $strongarm->value = '.uw-imp-dates-loader';
  $export['page_load_progress_elements'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'pathauto_node_uw_ct_important_dates_central_pattern';
  $strongarm->value = '[node:field-uw-imp-dates-academic-year]/[node:title]';
  $export['pathauto_node_uw_ct_important_dates_central_pattern'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'pathauto_taxonomy_term_uw_tax_imp_dates_academic_terms_pattern';
  $strongarm->value = '';
  $export['pathauto_taxonomy_term_uw_tax_imp_dates_academic_terms_pattern'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'pathauto_taxonomy_term_uw_tax_imp_dates_academic_year_pattern';
  $strongarm->value = '';
  $export['pathauto_taxonomy_term_uw_tax_imp_dates_academic_year_pattern'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'pathauto_taxonomy_term_uw_tax_imp_dates_keywords_pattern';
  $strongarm->value = '';
  $export['pathauto_taxonomy_term_uw_tax_imp_dates_keywords_pattern'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'pathauto_taxonomy_term_uw_tax_imp_dates_type_pattern';
  $strongarm->value = '';
  $export['pathauto_taxonomy_term_uw_tax_imp_dates_type_pattern'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'uuid_features_entity_taxonomy_term_uw_tax_important_dates_sites';
  $strongarm->value = 'uw_tax_important_dates_sites';
  $export['uuid_features_entity_taxonomy_term_uw_tax_important_dates_sites'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'uuid_features_entity_taxonomy_term_uw_tax_imp_dates_academic_terms';
  $strongarm->value = 'uw_tax_imp_dates_academic_terms';
  $export['uuid_features_entity_taxonomy_term_uw_tax_imp_dates_academic_terms'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'uuid_features_entity_taxonomy_term_uw_tax_imp_dates_academic_year';
  $strongarm->value = 'uw_tax_imp_dates_academic_year';
  $export['uuid_features_entity_taxonomy_term_uw_tax_imp_dates_academic_year'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'uuid_features_entity_taxonomy_term_uw_tax_imp_dates_audience';
  $strongarm->value = 'uw_tax_imp_dates_audience';
  $export['uuid_features_entity_taxonomy_term_uw_tax_imp_dates_audience'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'uuid_features_entity_taxonomy_term_uw_tax_imp_dates_calendar_count';
  $strongarm->value = 'uw_tax_imp_dates_calendar_count';
  $export['uuid_features_entity_taxonomy_term_uw_tax_imp_dates_calendar_count'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'uuid_features_entity_taxonomy_term_uw_tax_imp_dates_keywords';
  $strongarm->value = 'uw_tax_imp_dates_keywords';
  $export['uuid_features_entity_taxonomy_term_uw_tax_imp_dates_keywords'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'uuid_features_entity_taxonomy_term_uw_tax_imp_dates_remote_calendar';
  $strongarm->value = 'uw_tax_imp_dates_remote_calendar';
  $export['uuid_features_entity_taxonomy_term_uw_tax_imp_dates_remote_calendar'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'uuid_features_entity_taxonomy_term_uw_tax_imp_dates_remote_list_count';
  $strongarm->value = 'uw_tax_imp_dates_remote_list_count';
  $export['uuid_features_entity_taxonomy_term_uw_tax_imp_dates_remote_list_count'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'uuid_features_file_taxonomy_term_uw_tax_imp_dates_remote_calendar';
  $strongarm->value = 0;
  $export['uuid_features_file_taxonomy_term_uw_tax_imp_dates_remote_calendar'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'uuid_features_file_taxonomy_term_uw_tax_imp_dates_remote_list_count';
  $strongarm->value = 0;
  $export['uuid_features_file_taxonomy_term_uw_tax_imp_dates_remote_list_count'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'workbench_access';
  $strongarm->value = 'taxonomy';
  $export['workbench_access'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'workbench_access_allow_multiple';
  $strongarm->value = 0;
  $export['workbench_access_allow_multiple'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'workbench_access_auto_assign';
  $strongarm->value = 1;
  $export['workbench_access_auto_assign'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'workbench_access_custom_form';
  $strongarm->value = 1;
  $export['workbench_access_custom_form'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'workbench_access_label';
  $strongarm->value = 'Content access';
  $export['workbench_access_label'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'workbench_access_node_type_contact';
  $strongarm->value = 0;
  $export['workbench_access_node_type_contact'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'workbench_access_node_type_uwaterloo_custom_listing';
  $strongarm->value = 0;
  $export['workbench_access_node_type_uwaterloo_custom_listing'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'workbench_access_node_type_uw_blog';
  $strongarm->value = 0;
  $export['workbench_access_node_type_uw_blog'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'workbench_access_node_type_uw_ct_important_dates_central';
  $strongarm->value = 1;
  $export['workbench_access_node_type_uw_ct_important_dates_central'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'workbench_access_node_type_uw_ct_person_profile';
  $strongarm->value = 0;
  $export['workbench_access_node_type_uw_ct_person_profile'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'workbench_access_node_type_uw_embedded_call_to_action';
  $strongarm->value = 0;
  $export['workbench_access_node_type_uw_embedded_call_to_action'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'workbench_access_node_type_uw_embedded_facts_and_figures';
  $strongarm->value = 0;
  $export['workbench_access_node_type_uw_embedded_facts_and_figures'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'workbench_access_node_type_uw_embedded_timeline';
  $strongarm->value = 0;
  $export['workbench_access_node_type_uw_embedded_timeline'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'workbench_access_node_type_uw_event';
  $strongarm->value = 0;
  $export['workbench_access_node_type_uw_event'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'workbench_access_node_type_uw_home_page_banner';
  $strongarm->value = 0;
  $export['workbench_access_node_type_uw_home_page_banner'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'workbench_access_node_type_uw_image_gallery';
  $strongarm->value = 0;
  $export['workbench_access_node_type_uw_image_gallery'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'workbench_access_node_type_uw_news_item';
  $strongarm->value = 0;
  $export['workbench_access_node_type_uw_news_item'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'workbench_access_node_type_uw_project';
  $strongarm->value = 0;
  $export['workbench_access_node_type_uw_project'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'workbench_access_node_type_uw_promotional_item';
  $strongarm->value = 0;
  $export['workbench_access_node_type_uw_promotional_item'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'workbench_access_node_type_uw_service';
  $strongarm->value = 0;
  $export['workbench_access_node_type_uw_service'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'workbench_access_node_type_uw_site_footer';
  $strongarm->value = 0;
  $export['workbench_access_node_type_uw_site_footer'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'workbench_access_node_type_uw_special_alert';
  $strongarm->value = 0;
  $export['workbench_access_node_type_uw_special_alert'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'workbench_access_node_type_uw_web_form';
  $strongarm->value = 0;
  $export['workbench_access_node_type_uw_web_form'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'workbench_access_node_type_uw_web_page';
  $strongarm->value = 0;
  $export['workbench_access_node_type_uw_web_page'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'workbench_access_node_type_webform';
  $strongarm->value = 0;
  $export['workbench_access_node_type_webform'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'workbench_access_taxonomy';
  $strongarm->value = array(
    'uw_tax_important_dates_workbench_access' => 'uw_tax_important_dates_workbench_access',
    'uwaterloo_audience' => 0,
    'bibliography_keywords' => 0,
    'uw_blog_tags' => 0,
    'uwaterloo_contact_group' => 0,
    'uw_event_tags' => 0,
    'uw_event_type' => 0,
    'uw_tax_imp_dates_academic_terms' => 0,
    'uw_tax_imp_dates_academic_year' => 0,
    'uw_tax_imp_dates_keywords' => 0,
    'uw_tax_imp_dates_remote_list_count' => 0,
    'uw_tax_important_dates_sites' => 0,
    'uw_tax_imp_dates_type' => 0,
    'uw_tax_imp_dates_remote_calendar' => 0,
    'uwaterloo_profiles' => 0,
    'project_role' => 0,
    'project_status' => 0,
    'project_topic' => 0,
    'service_categories' => 0,
    'uw_service_tags' => 0,
    'strategic_alignment' => 0,
  );
  $export['workbench_access_taxonomy'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'xmlsitemap_settings_node_uw_ct_important_dates_central';
  $strongarm->value = array(
    'status' => '0',
    'priority' => '0.5',
  );
  $export['xmlsitemap_settings_node_uw_ct_important_dates_central'] = $strongarm;

  return $export;
}
