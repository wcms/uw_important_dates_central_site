<?php

/**
 * @file
 * uw_important_dates_central_site.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function uw_important_dates_central_site_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'access workbench access by role'.
  $permissions['access workbench access by role'] = array(
    'name' => 'access workbench access by role',
    'roles' => array(
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'workbench_access',
  );

  // Exported permission: 'administer important dates'.
  $permissions['administer important dates'] = array(
    'name' => 'administer important dates',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'uw_important_dates_central_site',
  );

  // Exported permission: 'administer workbench access'.
  $permissions['administer workbench access'] = array(
    'name' => 'administer workbench access',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'workbench_access',
  );

  // Exported permission: 'assign workbench access'.
  $permissions['assign workbench access'] = array(
    'name' => 'assign workbench access',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'workbench_access',
  );

  // Exported permission: 'batch update workbench access'.
  $permissions['batch update workbench access'] = array(
    'name' => 'batch update workbench access',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'workbench_access',
  );

  // Exported permission: 'create uw_ct_important_dates_central content'.
  $permissions['create uw_ct_important_dates_central content'] = array(
    'name' => 'create uw_ct_important_dates_central content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any uw_ct_important_dates_central content'.
  $permissions['delete any uw_ct_important_dates_central content'] = array(
    'name' => 'delete any uw_ct_important_dates_central content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own uw_ct_important_dates_central content'.
  $permissions['delete own uw_ct_important_dates_central content'] = array(
    'name' => 'delete own uw_ct_important_dates_central content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete terms in uw_tax_imp_dates_academic_terms'.
  $permissions['delete terms in uw_tax_imp_dates_academic_terms'] = array(
    'name' => 'delete terms in uw_tax_imp_dates_academic_terms',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'delete terms in uw_tax_imp_dates_academic_year'.
  $permissions['delete terms in uw_tax_imp_dates_academic_year'] = array(
    'name' => 'delete terms in uw_tax_imp_dates_academic_year',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'delete terms in uw_tax_imp_dates_keywords'.
  $permissions['delete terms in uw_tax_imp_dates_keywords'] = array(
    'name' => 'delete terms in uw_tax_imp_dates_keywords',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'delete terms in uw_tax_imp_dates_remote_calendar'.
  $permissions['delete terms in uw_tax_imp_dates_remote_calendar'] = array(
    'name' => 'delete terms in uw_tax_imp_dates_remote_calendar',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'delete terms in uw_tax_imp_dates_remote_list_count'.
  $permissions['delete terms in uw_tax_imp_dates_remote_list_count'] = array(
    'name' => 'delete terms in uw_tax_imp_dates_remote_list_count',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'delete terms in uw_tax_imp_dates_type'.
  $permissions['delete terms in uw_tax_imp_dates_type'] = array(
    'name' => 'delete terms in uw_tax_imp_dates_type',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'delete terms in uw_tax_important_dates_sites'.
  $permissions['delete terms in uw_tax_important_dates_sites'] = array(
    'name' => 'delete terms in uw_tax_important_dates_sites',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'delete terms in uw_tax_important_dates_workbench_access'.
  $permissions['delete terms in uw_tax_important_dates_workbench_access'] = array(
    'name' => 'delete terms in uw_tax_important_dates_workbench_access',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'edit any uw_ct_important_dates_central content'.
  $permissions['edit any uw_ct_important_dates_central content'] = array(
    'name' => 'edit any uw_ct_important_dates_central content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own uw_ct_important_dates_central content'.
  $permissions['edit own uw_ct_important_dates_central content'] = array(
    'name' => 'edit own uw_ct_important_dates_central content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit terms in uw_tax_imp_dates_academic_terms'.
  $permissions['edit terms in uw_tax_imp_dates_academic_terms'] = array(
    'name' => 'edit terms in uw_tax_imp_dates_academic_terms',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'edit terms in uw_tax_imp_dates_academic_year'.
  $permissions['edit terms in uw_tax_imp_dates_academic_year'] = array(
    'name' => 'edit terms in uw_tax_imp_dates_academic_year',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'edit terms in uw_tax_imp_dates_keywords'.
  $permissions['edit terms in uw_tax_imp_dates_keywords'] = array(
    'name' => 'edit terms in uw_tax_imp_dates_keywords',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'edit terms in uw_tax_imp_dates_remote_calendar'.
  $permissions['edit terms in uw_tax_imp_dates_remote_calendar'] = array(
    'name' => 'edit terms in uw_tax_imp_dates_remote_calendar',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'edit terms in uw_tax_imp_dates_remote_list_count'.
  $permissions['edit terms in uw_tax_imp_dates_remote_list_count'] = array(
    'name' => 'edit terms in uw_tax_imp_dates_remote_list_count',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'edit terms in uw_tax_imp_dates_type'.
  $permissions['edit terms in uw_tax_imp_dates_type'] = array(
    'name' => 'edit terms in uw_tax_imp_dates_type',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'edit terms in uw_tax_important_dates_sites'.
  $permissions['edit terms in uw_tax_important_dates_sites'] = array(
    'name' => 'edit terms in uw_tax_important_dates_sites',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'edit terms in uw_tax_important_dates_workbench_access'.
  $permissions['edit terms in uw_tax_important_dates_workbench_access'] = array(
    'name' => 'edit terms in uw_tax_important_dates_workbench_access',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'enter uw_ct_important_dates_central revision log entry'.
  $permissions['enter uw_ct_important_dates_central revision log entry'] = array(
    'name' => 'enter uw_ct_important_dates_central revision log entry',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'execute clone_action_clone'.
  $permissions['execute clone_action_clone'] = array(
    'name' => 'execute clone_action_clone',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'actions_permissions',
  );

  // Exported permission: 'execute node_publish_action'.
  $permissions['execute node_publish_action'] = array(
    'name' => 'execute node_publish_action',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'actions_permissions',
  );

  // Exported permission: 'execute node_unpublish_action'.
  $permissions['execute node_unpublish_action'] = array(
    'name' => 'execute node_unpublish_action',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'actions_permissions',
  );

  // Exported permission: 'execute uw_important_dates_central_site_change_academic_year'.
  $permissions['execute uw_important_dates_central_site_change_academic_year'] = array(
    'name' => 'execute uw_important_dates_central_site_change_academic_year',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'actions_permissions',
  );

  // Exported permission: 'merge uw_tax_imp_dates_academic_terms terms'.
  $permissions['merge uw_tax_imp_dates_academic_terms terms'] = array(
    'name' => 'merge uw_tax_imp_dates_academic_terms terms',
    'roles' => array(),
    'module' => 'term_merge',
  );

  // Exported permission: 'merge uw_tax_imp_dates_academic_year terms'.
  $permissions['merge uw_tax_imp_dates_academic_year terms'] = array(
    'name' => 'merge uw_tax_imp_dates_academic_year terms',
    'roles' => array(),
    'module' => 'term_merge',
  );

  // Exported permission: 'override uw_ct_important_dates_central authored by option'.
  $permissions['override uw_ct_important_dates_central authored by option'] = array(
    'name' => 'override uw_ct_important_dates_central authored by option',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_ct_important_dates_central authored on option'.
  $permissions['override uw_ct_important_dates_central authored on option'] = array(
    'name' => 'override uw_ct_important_dates_central authored on option',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_ct_important_dates_central promote to front page option'.
  $permissions['override uw_ct_important_dates_central promote to front page option'] = array(
    'name' => 'override uw_ct_important_dates_central promote to front page option',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_ct_important_dates_central published option'.
  $permissions['override uw_ct_important_dates_central published option'] = array(
    'name' => 'override uw_ct_important_dates_central published option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_ct_important_dates_central revision option'.
  $permissions['override uw_ct_important_dates_central revision option'] = array(
    'name' => 'override uw_ct_important_dates_central revision option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_ct_important_dates_central sticky option'.
  $permissions['override uw_ct_important_dates_central sticky option'] = array(
    'name' => 'override uw_ct_important_dates_central sticky option',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'view workbench access information'.
  $permissions['view workbench access information'] = array(
    'name' => 'view workbench access information',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'workbench_access',
  );

  // Exported permission: 'view workbench taxonomy pages'.
  $permissions['view workbench taxonomy pages'] = array(
    'name' => 'view workbench taxonomy pages',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'workbench_access',
  );

  return $permissions;
}
