<?php

/**
 * @file
 * uw_important_dates_central_site.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function uw_important_dates_central_site_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function uw_important_dates_central_site_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function uw_important_dates_central_site_node_info() {
  $items = array(
    'uw_ct_important_dates_central' => array(
      'name' => t('Important dates'),
      'base' => 'node_content',
      'description' => t('An important date, which has data specific to a date and multiple departments.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}

/**
 * Implements hook_paragraphs_info().
 */
function uw_important_dates_central_site_paragraphs_info() {
  $items = array(
    'uw_para_imp_date_details' => array(
      'name' => 'Important date details',
      'bundle' => 'uw_para_imp_date_details',
      'locked' => '1',
    ),
  );
  return $items;
}
