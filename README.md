# Important dates central site

This module will be used on the central site to store and serve all important
dates.

## Installation

Enable the module either through the GUI or the command line.  Once the module
is enabled the following must be run as well:
* Cache clear
* Feature revert
* Cache clear

This must be done so that certain features, such as Taxonomy terms, are
installed correctly.

## Post install

At least one academic year must be add to the taxonomy vocabulary Important
Dates Academic Year, in order to be able to enter any important dates.
