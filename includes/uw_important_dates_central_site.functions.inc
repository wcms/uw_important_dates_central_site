<?php

/**
 * @file
 * Code for the function of Important dates remote site.
 */

/**
 * Helper function.
 *
 * Get the calendar count and/or first calendar date entry.
 */
function _uw_important_dates_central_site_get_calendar_count($operator, $query_parameters, $dates = NULL) {

  // Get the view that counts the calendar entries.
  $view = views_get_view('uw_view_important_dates_calendar');
  $view->set_display('calendar_count');

  // Set the filter date.
  if (isset($query_parameters['navigate'])) {
    $filter_date = $query_parameters['navigate'];
  }
  else {
    $filter_date = date('Y-m');
  }

  // Get the filters from the view.
  $filters = $view->display_handler->get_option('filters');

  // If greater/less than, process with single date value.
  // If other operator, use operator and if bewteen set min/max date.
  if ($operator == '<' || $operator == '>') {

    // Set the operator to less than.
    $filters['field_uw_imp_dates_date_value']['operator'] = $operator;

    // Set the value of the filter in the view.
    $filters['field_uw_imp_dates_date_value']['value']['value'] = $filter_date;
  }
  else {

    // Set the operator.
    $filters['field_uw_imp_dates_date_value']['operator'] = $operator;

    // If the operator is between, set min and max dates.
    if ($operator == 'between') {

      // If there are min and max date, set them on the filters.
      if (isset($dates['max_date']) && isset($dates['min_date'])) {

        // Set the min and max dates on filter.
        $filters['field_uw_imp_dates_date_value']['value']['min'] = $dates['min_date'];
        $filters['field_uw_imp_dates_date_value']['value']['max'] = $dates['max_date'];
      }
      else {

        // Set the min and max dates on the filter.
        $filters['field_uw_imp_dates_date_value']['value']['min'] = $filter_date;
        $filters['field_uw_imp_dates_date_value']['value']['max'] = $filter_date;
      }
    }
  }

  // Set the filters for the view with updated date.
  $view->display_handler->set_option('filters', $filters);

  // Execute the view.
  $view->pre_execute();
  $view->execute();

  // Return the count of future entries from the results,
  // or the first entry of the view.
  if (count($view->result) == 0) {
    return;
  }
  else {
    return date('Y-m', strtotime($view->result[0]->field_data_field_uw_imp_dates_date_field_uw_imp_dates_date_v));
  }
}

/**
 * Helper function.
 *
 * Get the number of entries.
 */
function _uw_important_dates_central_site_get_list_view_entries($view_name, $display_id, $academic_years = NULL, $current_academic_year = NULL) {

  // If there are no academic years provided, get from settings.
  if ($academic_years == NULL) {

    // Get the academic years from settings.
    $future_academic_years = variable_get('uw_important_dates_academic_years');
  }

  // If there is no current academic year supplied, get from settings.
  if ($current_academic_year == NULL) {

    // Get the current academic year from settings.
    $current_academic_year = variable_get('uw_important_dates_current_academic_year');
  }

  $entry_found = FALSE;

  // Setup future and past academic years.
  foreach ($future_academic_years as $academic_year) {
    // If we are not in the current academic year, put in
    // the past academic years and unset from future.
    // If we are in the current academic year, place in past as well.
    if ($academic_year !== $current_academic_year) {
      $past_academic_years[] = $academic_year;
      unset($future_academic_years[$academic_year]);
    }
    else {
      $past_academic_years[] = $academic_year;
      break;
    }
  }

  // Step through each of the future academic years.
  foreach ($future_academic_years as $future_academic_year) {

    // Reset the filters variable.
    $filters = '';

    // If current academic year, check future and past dates.
    if ($future_academic_year == $current_academic_year) {

      // Set the academic year and dates filters.
      $filters['academic_year'] = $future_academic_year;
      $filters['date'] = 'Today';
    }
    else {

      // Set the academic year and dates filters.
      $filters['academic_year'] = $future_academic_year;
      $filters['date'] = 'Any';
    }

    // Get the list view using our function.
    $list_view = _uw_important_dates_central_site_get_view_with_exposed_filters('uw_view_important_dates', 'important_dates_list_entries', $filters);

    // If there are entries we can break out of this loop.
    if (count($list_view) > 0) {
      $entry_found = TRUE;
      break;
    }
  }

  // If there is no entry found in future, try the past.
  if (!$entry_found) {

    // Step through each of the past academic years and check for entry.
    foreach ($past_academic_years as $past_academic_year) {

      // Reset the filters variable.
      $filters = '';

      // Set the academic year and dates filters.
      $filters['academic_year'] = $past_academic_year;
      $filters['date'] = 'Any';

      // Get the list view using our function.
      $list_view = _uw_important_dates_central_site_get_view_with_exposed_filters('uw_view_important_dates', 'important_dates_list_entries', $filters);

      // If there are entries we can break out of this loop.
      if (count($list_view) > 0) {
        $entry_found = TRUE;
        break;
      }
    }
  }

  // If there is an entry found return it.
  if ($entry_found) {
    return $list_view;
  }
}

/**
 * Helper function.
 *
 * Get the results of a view with exposed filters.
 */
function _uw_important_dates_central_site_get_view_with_exposed_filters($view_name, $display, $filters = NULL) {

  // Get the view.
  $view = views_get_view($view_name);
  $view->set_display($display);

  // If there are filters add them to the view.
  if ($filters !== NULL) {

    // Get the exposed inputs from the view.
    $exposed_inputs = $view->get_exposed_input();

    // Step through each of the filters sent and
    // add the exposed inputs.
    foreach ($filters as $key => $value) {
      $exposed_inputs[$key] = $value;
    }

    // Set the exposed inputs on the view.
    $view->set_exposed_input($exposed_inputs);
  }

  // Execute the view.
  $view->execute();

  // Return the results of the view.
  return $view->result;
}

/**
 * Get the academic year tid for the configured current year.
 *
 * @return int|null
 *   The value of variable uw_important_dates_current_academic_year if set.
 *   Otherwise, the value returned by
 *   _uw_important_dates_central_site_get_current_academic_year_actual().
 */
function _uw_important_dates_central_site_get_current_academic_year() {
  return variable_get('uw_important_dates_current_academic_year', _uw_important_dates_central_site_get_current_academic_year_actual());
}

/**
 * Get the academic year tid for the actual current year.
 *
 * @return int|null
 *   The academic year tid for the current calendar year. NULL if not existing.
 */
function _uw_important_dates_central_site_get_current_academic_year_actual() {
  $current_academic_year = date('Y') . ' - ' . date('Y', strtotime('+1 year'));
  $term = taxonomy_get_term_by_name($current_academic_year, 'uw_tax_imp_dates_academic_year');
  if ($term) {
    return array_pop($term)->tid;
  }
}

/**
 * Helper function.
 *
 * Get the number of calendar entries.
 */
function _uw_important_dates_central_site_get_calendar_entry_count($arguments, $date_operator, $filter_date) {

  // Get the view that counts the calendar entries.
  $view = views_get_view('uw_view_important_dates_calendar');
  $view->set_display('calendar_count');

  // Set the arguments in the view.
  $view->set_arguments($arguments);

  // Get the filters from the view.
  $filters = $view->display_handler->get_option('filters');

  // Set the operator to less than.
  $filters['field_uw_imp_dates_date_value']['operator'] = $date_operator;

  // If the date filter is between, set min/max.
  // If not between, set operator and date value.
  if ($date_operator == 'between') {
    $filters['field_uw_imp_dates_date_value']['value']['min'] = $filter_date;
    $filters['field_uw_imp_dates_date_value']['value']['max'] = $filter_date;
  }
  else {
    $filters['field_uw_imp_dates_date_value']['value']['value'] = $filter_date;
  }

  // Set the filters for the view with updated date.
  $view->display_handler->set_option('filters', $filters);

  // Execute the view.
  $view->pre_execute();
  $view->execute();

  // Return the count of future entries from the results.
  return count($view->result);
}

/**
 * Helper function.
 *
 * Return the array with the list of date ranges.
 */
function _uw_important_dates_central_site_get_year_ranges($low, $high) {

  // Step through the range and get the dates in an array.
  foreach (range($low, $high) as $key) {
    $options[$key] = t('@range_dates', array('@range_dates' => $key));
  }

  return $options;
}

/**
 * Helper function.
 *
 * Get the options of a taxonomy term.
 */
function _uw_important_dates_central_site_taxonomy_terms_options($term_name) {

  if ($term_name == '') {
    return;
  }

  // Get the academic years from taxonomy.
  $options = array();
  $vid = taxonomy_vocabulary_machine_name_load('uw_tax_imp_dates_academic_year')->vid;
  $options_source = taxonomy_get_tree($vid);

  foreach ($options_source as $item) {
    $key = $item->tid;
    $value = $item->name;
    $options[$key] = $value;
  }

  return $options;
}

/**
 * Implements hook_action_form().
 *
 * Create academic year selection for bulk operation after selecting the
 * clone and change academic year operation.
 */
function uw_important_dates_central_site_change_academic_year_form() {

  // Get the academic years from taxonomy.
  $options = _uw_important_dates_central_site_taxonomy_terms_options('uw_tax_imp_dates_academic_year');

  if (empty($options)) {
    drupal_set_message(t('Error: No taxonomy term is added for academic year.'), 'error');
  }

  $form = array();

  $form['uw_important_dates_academic_year'] = array(
    '#type' => 'select',
    '#title' => t('Change academic year to:'),
    '#options' => $options,
    '#required' => TRUE,
  );

  // These are code copying from clone.module clone_action_clone_form function.
  $form['clone_context'] = array(
    '#tree' => TRUE,
  );

  $form['clone_context']['prefix_title'] = array(
    '#title' => t('Prefix title'),
    '#type' => 'checkbox',
    '#description' => t('Should cloned node tiles be prefixed?'),
    '#default_value' => isset($context['clone_context']['prefix_title']) ? $context['clone_context']['prefix_title'] : 1,
  );

  $form['clone_context']['substitute_from'] = array(
    '#title' => t('Substitute from string'),
    '#type' => 'textfield',
    '#description' => t('String (or regex) to substitute from in title and body.'),
    '#default_value' => isset($context['clone_context']['substitue_from']) ? $context['clone_context']['substitue_from'] : '',
  );

  $form['clone_context']['substitute_to'] = array(
    '#title' => t('Substitute to string'),
    '#type' => 'textfield',
    '#description' => t('String (or regex) to substitute to in title and body.'),
    '#default_value' => isset($context['clone_context']['substitue_to']) ? $context['clone_context']['substitue_to'] : '',
  );

  $form['clone_context']['substitute_case_insensitive'] = array(
    '#title' => t('Case insensitive substitution'),
    '#type' => 'checkbox',
    '#description' => t('Should the substituion match be case insensitive?'),
    '#default_value' => isset($context['clone_context']['substitute_case_insensitive']) ? $context['clone_context']['substitute_case_insensitive'] : NULL,
  );

  return $form;
}

/**
 * Implements hook_action().
 *
 * Implement the clone and change academic year action.
 */
function uw_important_dates_central_site_change_academic_year($original_node, $context) {

  // These are code copying from clone.module clone_action_clone.
  module_load_include('inc', 'clone', 'clone.pages');

  if (clone_is_permitted($original_node->type)) {

    $val = !empty($context['clone_context']) ? $context['clone_context'] : array();
    $node = _clone_node_prepare($original_node, !empty($val['prefix_title']));

    // Modify academic year field with selected option saved
    // in hook_action_form.
    $node->field_uw_imp_dates_academic_year['und'][0]['tid'] = $context['uw_important_dates_academic_year'];

    if (isset($val['substitute_from']) && strlen($val['substitute_from']) && isset($val['substitute_to'])) {

      $i = (!empty($val['substitute_case_insensitive']) ? 'i' : '');
      $pattern = '#' . strtr($val['substitute_from'], array('#' => '\#')) . '#' . $i;

      foreach (array('title') as $property) {

        $new = preg_replace($pattern, $val['substitute_to'], $node->{$property});

        if ($new) {
          $node->{$property} = $new;
        }
      }

      foreach (array('body') as $property) {

        foreach ($node->{$property} as $lang => $row) {

          foreach ($row as $delta => $data) {

            foreach (array('value', 'summary') as $key) {

              if (isset($node->{$property}[$lang][$delta][$key])) {

                $new = preg_replace($pattern, $val['substitute_to'], $node->{$property}[$lang][$delta][$key]);

                if ($new) {
                  $node->{$property}[$lang][$delta][$key] = $new;
                }
              }
            }
          }
        }
      }
    }

    // Let other modules do special fixing up.
    $context = array(
      'method' => 'action',
      'original_node' => $original_node,
      'clone_context' => $val,
    );

    drupal_alter('clone_node', $node, $context);

    node_save($node);

    if (module_exists('rules')) {
      rules_invoke_event('clone_node', $node, $original_node);
    }
  }
  else {

    drupal_set_message(t('Clone failed for %title : not permitted for nodes of type %type', array('%title' => $original_node->title, '%type' => $original_node->type)), 'warning');
  }
}
