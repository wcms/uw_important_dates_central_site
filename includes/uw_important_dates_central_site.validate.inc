<?php

/**
 * @file
 * Code for the validations of Important dates remote site.
 */

/**
 * Helper function.
 *
 * Validate that the entered date is in the academic year.
 */
function _uw_important_dates_central_site_node_form_validate($form, &$form_state) {

  // If there are dates entered, process them.
  if (isset($form_state['values']['field_uw_imp_date_details'])) {

    // Step through each date and process them.
    foreach ($form_state['values']['field_uw_imp_date_details'] as $imp_date_details) {

      // Step through each date and ensure that it is a date.
      foreach ($imp_date_details as $key => $details) {

        // Check that we have a date (i.e. key is an integer).
        if (is_int($key)) {

          // If there are dates, process them.
          if (isset($details['field_uw_imp_dates_date'])) {

            // Get the academic year term id from the form values.
            $academic_year_tid = $form_state['values']['field_uw_imp_dates_academic_year']['und'][0]['tid'];

            // Get the term object using the tid.
            $academic_year = taxonomy_term_load($academic_year_tid);

            // Split the term name to get the academic years
            // that we need to check.
            $academic_years = explode(' - ', $academic_year->name);

            // Step through each date and get the date ranges, if there are any.
            foreach ($details['field_uw_imp_dates_date'] as $dates) {

              // Step through the actual dates.
              foreach ($dates as $date) {

                // Set the value of the start date.
                $date_ranges[] = $date['value'];

                // Check if we have and end date to check, if so track it.
                if ($date['value'] !== $date['value2']) {
                  $date_ranges[] = $date['value2'];
                }

                // Step through each date range and check in academic year.
                foreach ($date_ranges as $date_range) {

                  // Split the date entered, to check the year.
                  $split_date = explode('-', $date_range);

                  // If the entered date year is not in academic years,
                  // show error.
                  if (!in_array($split_date[0], $academic_years)) {

                    // The field name where we want to set the error.
                    $field_name = 'field_uw_imp_date_details][und][' . $key . '][field_uw_imp_dates_date';

                    // Set the error.
                    form_set_error($field_name, 'The date(s) entered are not in the academic year selected.');
                  }
                }
              }
            }
          }
        }
      }
    }
  }
}

/**
 * Helper function.
 *
 * Ensure that the academic year is entered correctly.
 */
function _uw_important_dates_central_site_validate_academic_year($form, &$form_state) {

  // If there is an academic year entered, process it.
  if (isset($form_state['values']['name'])) {

    // Regular expression pattern that we are looking for.
    $pattern = '/[0-9]{4}+\s-+\s[0-9]{4}/';

    // Perform the test and if it fails, display form error message.
    if (!preg_match($pattern, $form_state['values']['name'])) {
      form_set_error('name', 'You must enter an academic year in the format YYYY - YYYY.');
    }
  }
}

/**
 * Helper function.
 *
 * Form validation for important date settings.
 */
function _uw_important_dates_central_site_settings_validate($form, &$form_state) {

  // If there are values set for archive link text and the actual link,
  // process the archive link text and link.
  if (isset($form_state['values']['uw_archive_link_text']) || isset($form_state['values']['uw_archive_link'])) {

    // If either are not blank, continue to process.
    if ($form_state['values']['uw_archive_link_text'] !== '' || $form_state['values']['uw_archive_link'] !== '') {

      // If the link text is blank, show error on link text.
      if ($form_state['values']['uw_archive_link_text'] == '') {
        form_set_error('uw_archive_link_text', 'Archive link text must not be blank.');
      }
      // If the link is blank, show error on the link.
      elseif ($form_state['values']['uw_archive_link'] == '') {
        form_set_error('uw_archive_link', 'Archive link must not be blank.');
      }
      // Both link text and link have something in them,
      // continue to process and validate link.
      else {

        // Check if URL is valid, if not show error on link.
        if (!valid_url($form_state['values']['uw_archive_link'], TRUE)) {
          form_set_error('uw_archive_link', 'Archive link must valid and absolute.');
        }
      }
    }
  }

  // If the currrent academic year is not in the academic years
  // to be used in filters, show error and do not save form.
  if (!in_array($form_state['values']['uw_important_dates_current_academic_year'], $form_state['values']['uw_important_dates_academic_years'])) {

    // Set the error.
    form_set_error('uw_important_dates_academic_years', 'The current academic year must be included in the academic years to be used in filters.');
  }
}
