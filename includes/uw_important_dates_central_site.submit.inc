<?php

/**
 * @file
 * Code for the submit functions of Important dates remote site.
 */

/**
 * Helper function.
 *
 * Custom callback for reset button on calendar view.
 */
function _uw_important_dates_central_site_calendar_submit($form, $form_state) {
  drupal_goto('important-dates/calendar');
}

/**
 * Implements hook_action_submit().
 *
 * Submit academic year selection, save the selected year to $context.
 */
function uw_important_dates_central_site_change_academic_year_submit($form, $form_state) {

  $return = array();
  $return['uw_important_dates_academic_year'] = $form_state['values']['uw_important_dates_academic_year'];
  $return['clone_context'] = $form_state['values']['clone_context'];

  return $return;
}
