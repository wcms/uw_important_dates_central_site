<?php

/**
 * @file
 * Functions relating to archives.
 */

/**
 * Helper function.
 *
 * Page callback for archive for year only.
 */
function _uw_important_dates_central_site_archive_by_year() {

  // The arguments from the URL.
  $args = arg();

  // If there are three arguments, show page, if not throw 404.
  if (count($args) == 3) {

    // The array used for the page.
    $page = array();

    // Variable to be used if this is valid archive.
    $valid_archive = FALSE;

    // Important dates wrapper.
    $page['important-dates'] = array(
      '#type' => 'container',
      '#attributes' => array('class' => array('uw-important-dates')),
    );

    // The archive wrapper.
    $page['important-dates']['archive'] = array(
      '#type' => 'container',
      '#attributes' => array('class' => array('uw-important-dates-archive')),
    );

    // Add the archive message wrapper to the page.
    $page['important-dates']['archive']['messages'] = array(
      '#type' => 'container',
      '#attributes' => array('class' => array('view-id-uw_ct_special_alert_block')),
    );

    // Get the academic year.
    $academic_year_name = str_replace('-', ' - ', end($args));

    // Get the academic year tid using our function.
    $academic_year_tid = _uw_important_dates_central_site_get_tid_by_name($academic_year_name, 'uw_tax_imp_dates_academic_year');

    // If there is a academic year tid, process it.
    if ($academic_year_tid) {

      // Load in the academic term taxonomy term.
      $academic_year_term = taxonomy_term_load($academic_year_tid);

      // Get the current academic year from settings.
      $current_academic_year = taxonomy_term_load(variable_get('uw_important_dates_current_academic_year'));

      // If the academic year tid is less than the current from settings,
      // we have a valid archive.
      if ($academic_year_term->name < $current_academic_year->name) {
        $valid_archive = TRUE;
      }
    }

    // If we have a valid archive, set the historical message and list. If we do
    // not, show message about invalid archive.
    if ($valid_archive) {

      // Get the years from the academic year.
      $years = explode('-', $academic_year_name);

      // Set the date range to be displayed.
      $display_date = 'Sep. 1, ' . $years[0] . ' to Aug. 31, ' . $years[1];

      // Display the date range.
      $page['important-dates']['archive']['dates']['date'] = array(
        '#type' => 'markup',
        '#markup' => '<h2>' . $display_date . '</h2>',
      );

      // The archive message.
      $archive_message = isset(variable_get('uw_archive_message')['value']) && variable_get('uw_archive_message')['value'] !== '' ? variable_get('uw_archive_message')['value'] : 'These important dates are provided for historical purpose.';

      // Add the archive message to the page.
      $page['important-dates']['archive']['messages']['message'] = array(
        '#type' => 'markup',
        '#markup' => '<div class="clearfix"><p>' . $archive_message . '</p></div>',
      );

      // The actual data for the archive.
      $page['important-dates']['archive']['list'] = array(
        '#type' => 'markup',
        '#markup' => views_embed_view('uw_view_important_dates', 'important_dates_archive_by_year', $academic_year_tid),
      );
    }
    else {

      // Throw 404.
      drupal_not_found();
    }
  }
  else {

    // Throw 404.
    drupal_not_found();
  }

  // Return the form.
  return $page;
}

/**
 * Helper function.
 *
 * Page callback for archive for year only.
 */
function _uw_important_dates_central_site_archive_by_year_and_term() {

  // The arguments from the URL.
  $args = arg();

  // If there are 4 arguments, process it, if not 404.
  if (count($args) == 4) {

    // The count of the arguments.
    $arg_count = count($args);

    // The array used for the page.
    $page = array();

    // Variable to be used if this is valid archive.
    $valid_archive = FALSE;

    // Important dates wrapper.
    $page['important-dates'] = array(
      '#type' => 'container',
      '#attributes' => array('class' => array('uw-important-dates')),
    );

    // The archive wrapper.
    $page['important-dates']['archive'] = array(
      '#type' => 'container',
      '#attributes' => array('class' => array('uw-important-dates-archive')),
    );

    // Add the archive message wrapper to the page.
    $page['important-dates']['archive']['messages'] = array(
      '#type' => 'container',
      '#attributes' => array('class' => array('view-id-uw_ct_special_alert_block')),
    );

    // Get the academic year.
    $academic_year_name = str_replace('-', ' - ', $args[$arg_count - 2]);

    // Get the academic year tid using our function.
    $academic_year_tid = _uw_important_dates_central_site_get_tid_by_name($academic_year_name, 'uw_tax_imp_dates_academic_year');

    // If there is a academic year tid, process it.
    if ($academic_year_tid) {

      // Load in the academic term taxonomy term.
      $academic_year_term = taxonomy_term_load($academic_year_tid);

      // Get the current academic year from settings.
      $current_academic_year = taxonomy_term_load(variable_get('uw_important_dates_current_academic_year'));

      // If the academic year tid is less than the current from settings,
      // we have a valid archive.
      if ($academic_year_term->name < $current_academic_year->name) {

        // Get the academic term tid using our function.
        $academic_term_tid = _uw_important_dates_central_site_get_tid_by_name($args[$arg_count - 1], 'uw_tax_imp_dates_academic_terms');

        // If there is an academic term, we have a valid archive.
        if ($academic_term_tid) {
          $valid_archive = TRUE;
        }
      }
    }

    // If we have valid archive, display data, if not 404.
    if ($valid_archive) {

      // Get the archive message.
      $archive_message = isset(variable_get('uw_archive_message')['value']) && variable_get('uw_archive_message')['value'] !== '' ? variable_get('uw_archive_message')['value'] : 'These important dates are provided for historical purpose.';

      // Add the archive message to the page.
      $page['important-dates']['archive']['messages']['message'] = array(
        '#type' => 'markup',
        '#markup' => '<div class="clearfix"><p>' . $archive_message . '</p></div>',
      );

      // Wrapper used for displaying the date range and term.
      $page['important-dates']['archive']['dates'] = array(
        '#type' => 'container',
        '#attributes' => array('class' => array('uw-important-dates-dates')),
      );

      // Get the years from the academic year.
      $years = explode('-', $academic_year_name);

      // Set the date range and term to be displayed.
      $display_date = 'Sep. 1, ' . $years[0] . ' to Aug. 31, ' . $years[1] . ' - ' . ucfirst($args[$arg_count - 1]);

      // Display the date range and term.
      $page['important-dates']['archive']['dates']['date'] = array(
        '#type' => 'markup',
        '#markup' => '<h2>' . $display_date . '</h2>',
      );

      // Display the archive data.
      $page['important-dates']['archive']['list'] = array(
        '#type' => 'markup',
        '#markup' => views_embed_view('uw_view_important_dates', 'important_dates_archive_by_year_and_term', $academic_year_tid, $academic_term_tid),
      );
    }
    else {

      // Throw 404.
      drupal_not_found();
    }
  }
  else {

    // Throw 404.
    drupal_not_found();
  }

  return $page;
}

/**
 * Helper function.
 *
 * Get the taxonomy term id using the taxonomy name.
 * This will only return the first term that is loaded.
 */
function _uw_important_dates_central_site_get_tid_by_name($term_name, $vocab_id = NULL) {

  // Get the terms by name.
  $terms = taxonomy_get_term_by_name($term_name, $vocab_id);

  // If there are terms process them, if not return NULL.
  if (isset($terms) && count($terms) > 0) {

    // Step through each term and get the tid.
    foreach ($terms as $term) {
      $tid = $term->tid;
      break;
    }

    // Return the tid.
    return $tid;
  }
  else {
    return;
  }
}

/**
 * Helper function.
 *
 * Create a link to the list view.
 */
function _uw_important_dates_central_site_create_archive_link($form, &$form_state) {

  // Get the query parameters.
  $query_parameters = uw_important_dates_get_query_parameters();

  // Wrapper for link to important dates settings.
  $form['links'] = array(
    '#type' => 'container',
    '#attributes' => array('class' => array('uw-important-dates-links')),
  );

  // Link back to important dates settings.
  $form['links']['link'] = array(
    '#type' => 'markup',
    '#markup' => l(t('Important dates settings'), 'admin/config/system/uw_important_dates_remote_site_settings', array('attributes' => array('class' => array('uw-important-dates-links-link')))),
  );

  // Wrapper for filters.
  $form['filters'] = array(
    '#type' => 'container',
    '#attributes' => array('class' => array('uw-create-list-link-filters')),
  );

  // The current academic year from settings.
  $academic_years_setting = taxonomy_term_load(variable_get('uw_important_dates_current_academic_year'));

  $academic_years_setting = $academic_years_setting->name;

  // Get the academic year vocab and tree.
  $academic_years_vocab = taxonomy_vocabulary_machine_name_load('uw_tax_imp_dates_academic_year');
  $academic_years = taxonomy_get_tree($academic_years_vocab->vid);

  // Step through each academic year and process it.
  foreach ($academic_years as $academic_year) {

    // If the academic year is less than the current from settings,
    // add the options to be used in creating the archive link.
    if ($academic_year->name < $academic_years_setting) {

      // Separate the years in the academic year.
      $years = explode(' - ', $academic_year->name);

      // Set the display for the academic year.
      $key = str_replace(' ', '', $academic_year->name);
      $value = 'Sep. 1, ' . $years[0] . ' to Aug. 31, ' . $years[1];
      $academic_years_options[$key] = $value;
    }
  }

  // The academic year filter drop down.
  $form['filters']['academic_year'] = array(
    '#type' => 'select',
    '#title' => 'Academic year',
    '#options' => $academic_years_options,
    '#default_value' => isset($query_parameters['academic_year']) ? $query_parameters['academic_year'] : $academic_years_setting,
    '#prefix' => '<div class="important-dates-filters-filter">',
    '#suffix' => '</div>',
    '#attributes' => array('class' => array('important-dates-filters__academic-year')),
  );

  // Get the academic terms vocab and tree.
  $academic_terms_vocab = taxonomy_vocabulary_machine_name_load('uw_tax_imp_dates_academic_terms');
  $academic_terms = taxonomy_get_tree($academic_terms_vocab->vid);

  // Step through each academic year and setup options array.
  foreach ($academic_terms as $academic_term) {
    $academic_terms_options[$academic_term->name] = $academic_term->name;
  }

  // The academic term filter drop down.
  $form['filters']['academic_term'] = array(
    '#type' => 'select',
    '#title' => 'Academic term',
    '#options' => $academic_terms_options,
    '#empty_option' => '- Any -',
    '#default_value' => isset($query_parameters['academic_term']) ? $query_parameters['academic_term'] : '',
    '#prefix' => '<div class="important-dates-filters-filter">',
    '#suffix' => '</div>',
    '#attributes' => array('class' => array('important-dates-filters__academic-term')),
  );

  // Submit button.
  $form['filters']['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Create',
  );

  // Add submit handler.
  $form['#submit'][] = '_uw_important_dates_central_site_create_archive_link_submit';

  return $form;
}

/**
 * Helper function.
 *
 * Submit handler for create list link.
 */
function _uw_important_dates_central_site_create_archive_link_submit($form, &$form_state) {

  // Variable to store parameters.
  $query_parameters = array();

  // URL to be used.
  $archive_url = 'important-dates/archive/';

  // If the academic year has a value, add to query parameters.
  if (isset($form_state['values']['academic_year']) && $form_state['values']['academic_year'] !== '') {

    // Add to the archive URL.
    $archive_url .= $form_state['values']['academic_year'];

    // Set the query parameters.
    $query_parameters['academic_year'] = $form_state['values']['academic_year'];
  }

  // If the academic term has a value, add to query parameters.
  if (isset($form_state['values']['academic_term']) && $form_state['values']['academic_term'] !== '') {

    // Add to the archive URL.
    $archive_url .= '/' . $form_state['values']['academic_term'];

    // Set the query parameters.
    $query_parameters['academic_term'] = $form_state['values']['academic_term'];
  }

  // Generate the url with the parameters.
  $url = url($archive_url, array('absolute' => TRUE));

  // Go to the URL with the parameters.
  drupal_set_message($url, 'status');

  // Go to the settings page with parameters.
  drupal_goto('admin/config/system/uw_important_dates_create_archive_link', array('query' => $query_parameters));
}
