<?php

/**
 * @file
 * Code for the function of Important dates remote site.
 */
/**
 * Implements hook_form_FORM_ID_alter().
 *
 * Add default academic year to form.
 */
function uw_important_dates_central_site_form_uw_ct_important_dates_central_node_form_alter(&$form, &$form_state, $form_id) {

  // Ensure that we allow for the path alias to be changed,
  // and then do not let user change it.
  $form['path']['pathauto']['#default_value'] = 1;
  $form['path']['pathauto']['#disabled'] = TRUE;
  $form['path']['alias']['#attributes'] = array('readonly' => 'readonly');

  // Forcing the important date as a wide page.
  $form['uw_page_settings_node_edit']['uw_page_settings_node_enable']['#default_value'] = 1;
  $form['uw_page_settings_node_edit']['uw_page_settings_node_enable']['#disabled'] = TRUE;

  // Set the default value of the academic year when creating new node.
  // If the default academic year is set.
  if (current_path() == 'node/add/uw-ct-important-dates-central' && variable_get('uw_important_dates_default_academic_year')) {
    $form['field_uw_imp_dates_academic_year'][LANGUAGE_NONE]['#default_value'] = variable_get('uw_important_dates_default_academic_year');
  }

  // Unset uw_tax_important_dates_workbench_access from options.
  if (isset($form['workbench_access']['workbench_access']['#options'])) {
    unset($form['workbench_access']['workbench_access']['#options']['uw_tax_important_dates_workbench_access']);
  }

  // Set the first taxonomy term as default.
  if (!isset($form['workbench_access']['workbench_access']['#default_value']) || count($form['workbench_access']['workbench_access']['#default_value']) == 0) {
    foreach ($form['workbench_access']['workbench_access']['#options'] as $key => $item) {
      if ($key != NULL && $key != 'uw_tax_important_dates_workbench_access') {
        $form['workbench_access']['workbench_access']['#default_value'] = array($key);
        break;
      }
    }
  }

  // Add the form validation for dates entered in academic year selected.
  $form['#validate'][] = '_uw_important_dates_central_site_node_form_validate';
}

/**
 * Helper function.
 *
 * Form callback for important dates config form.
 */
function _uw_important_dates_central_site_settings_form($form, &$form_state) {

  // Get the academic years from taxonomy.
  $options = _uw_important_dates_central_site_taxonomy_terms_options('uw_tax_imp_dates_academic_year');

  // Wrapper for the create archive link.
  $form['uw_create_archive_link_wrapper'] = array(
    '#type' => 'container',
    '#attributes' => array('class' => array('uw-create-archive-link-wrapper')),
  );

  // Create archive link.
  $form['uw_create_archive_link_wrapper']['create_archive_link'] = array(
    '#type' => 'markup',
    '#markup' => l(t('Create archive link'), 'admin/config/system/uw_important_dates_create_archive_link', array('attributes' => array('class' => array('uw-create-archive-link')))),
  );

  // Fieldset for academic year.
  $form['uw_academic_year'] = array(
    '#type' => 'fieldset',
    '#title' => t('Academic Year Settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  // Select drop down for current academic year.
  // This will be the academic year displayed on the important dates site.
  $form['uw_academic_year']['uw_important_dates_current_academic_year'] = array(
    '#type' => 'select',
    '#title' => t('Current academic year'),
    '#multiple' => FALSE,
    '#options' => $options,
    '#description' => t('Select the current academic year, used for when displaying important dates.'),
    '#default_value' => variable_get('uw_important_dates_current_academic_year'),
    '#required' => TRUE,
    '#empty_option' => '- Select a value -',
  );

  // Select drop down for academic years to used in filters.
  $form['uw_academic_year']['uw_important_dates_academic_years'] = array(
    '#type' => 'select',
    '#title' => t('Academic years to be used in filters'),
    '#multiple' => TRUE,
    '#options' => $options,
    '#description' => t('Select the academic year(s) that are to be used in the filters.'),
    '#default_value' => variable_get('uw_important_dates_academic_years'),
    '#required' => TRUE,
    '#empty_option' => '- Select a value -',
  );

  // Select drop down for default academic year.
  // This will be the academic year used for defaulting,
  // when entering important dates.
  $form['uw_academic_year']['uw_important_dates_default_academic_year'] = array(
    '#type' => 'select',
    '#title' => t('Default academic year'),
    '#multiple' => FALSE,
    '#options' => $options,
    '#description' => t('Select the default academic year, used for when entering important dates.'),
    '#default_value' => variable_get('uw_important_dates_default_academic_year'),
    '#required' => TRUE,
    '#empty_option' => '- Select a value -',
  );

  // Fieldset for academic year.
  $form['uw_calendar'] = array(
    '#type' => 'fieldset',
    '#title' => t('Calendar Settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  // Set the start year based on the variable or current year.
  $start_year = variable_get('uw_start_year_calendar', date('Y'));

  // The options for the start year.
  $start_year_options = _uw_important_dates_central_site_get_year_ranges($start_year - 4, $start_year + 4);

  // Select drop down for the start year calendar view.
  $form['uw_calendar']['uw_start_year_calendar'] = array(
    '#type' => 'select',
    '#title' => t('Start year for calendar view'),
    '#multiple' => FALSE,
    '#options' => $start_year_options,
    '#description' => t('Select the start year when viewing the calendar.'),
    '#default_value' => variable_get('uw_start_year_calendar'),
    '#required' => TRUE,
    '#empty_option' => '- Select a value -',
  );

  // Set the end year based on the variable or current year.
  $end_year = variable_get('uw_end_year_calendar', date('Y'));

  // The options for the end year.
  $end_year_options = _uw_important_dates_central_site_get_year_ranges($end_year - 6, $end_year + 6);

  // Select drop down for the start year calendar view.
  $form['uw_calendar']['uw_end_year_calendar'] = array(
    '#type' => 'select',
    '#title' => t('End year for calendar view'),
    '#multiple' => FALSE,
    '#options' => $end_year_options,
    '#description' => t('Select the end year when viewing the calendar.'),
    '#default_value' => variable_get('uw_end_year_calendar'),
    '#required' => TRUE,
    '#empty_option' => '- Select a value -',
  );

  // Fieldset for archive settings.
  $form['uw_archive'] = array(
    '#type' => 'fieldset',
    '#title' => t('Archive Settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['uw_archive']['uw_archive_message_wrapper'] = array(
    '#type' => 'fieldset',
    '#title' => t('Archive Message'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
    '#description' => 'Enter a message to be displayed when on an archive page.  If left blank, the message will display as: "These important dates are provided for historical purpose.".',
  );

  // Message for archive pages.
  $form['uw_archive']['uw_archive_message_wrapper']['uw_archive_message'] = array(
    '#type' => 'text_format',
    '#default_value' => variable_get('uw_archive_message') ? variable_get('uw_archive_message')['value'] : '',
    '#format' => 'uw_tf_basic',
  );

  // Set variable uw_important_dates_archive_exclude: Dates having these
  // taxonomy terms are to be excluded from archives.
  $uw_tax_imp_dates_type = taxonomy_vocabulary_machine_name_load('uw_tax_imp_dates_type');
  $important_dates_types = [];
  foreach (taxonomy_term_load_multiple([], ['vid' => $uw_tax_imp_dates_type->vid]) as $tid => $term) {
    $important_dates_types[$tid] = $term->name;
  }
  $form['uw_archive']['uw_important_dates_archive_exclude'] = [
    '#type' => 'checkboxes',
    '#title' => t('Date types to exclude from archive'),
    '#options' => $important_dates_types,
    '#default_value' => variable_get('uw_important_dates_archive_exclude', []),
  ];

  // Fieldset for archive link settings.
  $form['uw_archive']['uw_archive_link_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Archive Link Settings'),
    '#collapsible' => FALSE,
  );

  // Archive link text.
  $form['uw_archive']['uw_archive_link_settings']['uw_archive_link_text'] = array(
    '#type' => 'textfield',
    '#title' => 'Archive link text',
    '#default_value' => variable_get('uw_archive_link_text'),
    '#description' => 'Enter the text that will be in the link to the archive list page.',
  );

  // Actual archive link, must be fully qualified URL.
  $form['uw_archive']['uw_archive_link_settings']['uw_archive_link'] = array(
    '#type' => 'textfield',
    '#title' => 'Archive link',
    '#default_value' => variable_get('uw_archive_link'),
    '#description' => t('Enter a full URL to the archive page, i.e. https://uwaterloo.ca/important-dates/archives-list.'),
  );

  // Add validation to the form.
  $form['#validate'][] = '_uw_important_dates_central_site_settings_validate';

  return system_settings_form($form);
}

/**
 * Implements hook_form_FORM_ID_alter().
 *
 * Unset -All- for dates filter.
 * Disable date filter if Academic year is '- Any -'.
 * Pass variable tid to js.
 */
function uw_important_dates_central_site_form_views_exposed_form_alter(&$form, &$form_state, $form_id) {

  global $_uw_important_dates_remote_site_num_of_entries;

  // If on the list view, change the academic years to
  // include start and end full dates.
  if ($form['#id'] == 'views-exposed-form-uw-view-important-dates-important-dates-list-page' && isset($form['academic_year'])) {

    // Add page load progress class to search and reset buttons.
    $form['submit']['#attributes']['class'][] = 'uw-imp-dates-loader';
    $form['reset']['#attributes']['class'][] = 'uw-imp-dates-loader';

    // Get the academic years to be used in the filters.
    $academic_years = variable_get('uw_important_dates_academic_years', []);

    // ISTWCMS-3024.
    // Step through each academic year in the filter.
    // Check if it is in the settings and is an integer.
    // If so, keep it in the filters, if not remove it.
    foreach ($form['academic_year']['#options'] as $key => $value) {
      if (in_array($key, $academic_years) && is_int($key)) {
        $years = explode(' - ', $value);
        $form['academic_year']['#options'][$key] = 'Sep 1, ' . $years[0] . ' to Aug 31, ' . $years[1];
      }
      else {
        unset($form['academic_year']['#options'][$key]);
      }
    }
  }

  if ($form['#id'] == 'views-exposed-form-uw-view-important-dates-calendar-month') {

    // Add page load progress class to search and reset buttons.
    $form['submit']['#attributes']['class'][] = 'uw-imp-dates-loader';
    $form['reset']['#attributes']['class'][] = 'uw-imp-dates-loader';

    // Setting the reset button to use custom callback.
    $form['reset']['#name'] = 'calendar_reset';
    $form['reset']['#type'] = 'submit';
    $form['reset']['#submit'][] = '_uw_important_dates_central_site_calendar_submit';

    // If there are no entries, then display No results message.
    if ($_uw_important_dates_remote_site_num_of_entries == 0) {
      $form['#suffix'] = '<div class="important-dates-calendar-message">No results found.</div>';
    }
  }

  if ($form['#id'] == 'views-exposed-form-uw-view-important-dates-important-dates-list-page') {

    // Add page load progress class to search and reset buttons.
    $form['submit']['#attributes']['class'][] = 'uw-imp-dates-loader';
    $form['reset']['#attributes']['class'][] = 'uw-imp-dates-loader';

    // If there are no entries, show the no results found message.
    if ($_uw_important_dates_remote_site_num_of_entries == 0) {
      $form['#suffix'] = '<div class="important-dates-calendar-message">No results found</div>';
    }

    // Unset -All- of dates filter.
    if (isset($form['date'])) {
      unset($form['date']['#options']['All']);
    }

    // Prepare tid based on variable or actual current year.
    $current_acadmic_year_tid = _uw_important_dates_central_site_get_current_academic_year();

    // Prepare tid2 based on actual current year.
    $actual_current_year_tid = _uw_important_dates_central_site_get_current_academic_year_actual();

    // Pass tid and tid2 which will be used in js.
    // In order to disable or enable dates filter.
    drupal_add_js(array('uw_important_dates_central_site' => array('tid' => $current_acadmic_year_tid)), array('type' => 'setting'));
    drupal_add_js(array('uw_important_dates_central_site' => array('tid2' => $actual_current_year_tid)), array('type' => 'setting'));

    // Set Any in Dates filter if not current academic year.
    $query_parameters = uw_important_dates_get_query_parameters();

    $current_url = substr(request_uri(), -20, 20);
    if ($current_url == 'important-dates/list' ||
      strpos(request_uri(), 'important-dates/list?date=Today&page=') !== FALSE ||
      strpos(request_uri(), 'important-dates/list?date=Today')) {
      // Set current academic year for landing list page.
      $form_state['input']['academic_year'] = _uw_important_dates_central_site_get_current_academic_year();
    }
    else {

      // Disable dates filter if NOT selecting current academic year.
      if (isset($form_state['input']['academic_year']) && $form_state['input']['academic_year'] !== variable_get('uw_important_dates_current_academic_year')) {
        $form['date']['#attributes']['disabled'] = TRUE;
      }
    }

    if (isset($query_parameters['academic_year']) &&
      ($query_parameters['academic_year'] !== _uw_important_dates_central_site_get_current_academic_year())) {
      $form_state['input']['date'] = 'Any';
    }
  }

  if (strpos($form['#action'], 'admin/workbench/create') !== FALSE) {
    // Unset sections from management views of all content types
    // if workbench access editorial assignments are not done.
    // Unset sections from management views of all content types
    // except important dates if they are done.
    if ((isset($form['access_id']['#options']) && empty($form['access_id']['#options']))
      || ($form['#id'] !== 'views-exposed-form-manage-important-dates-manage-important-dates')) {
      unset($form['access_id']);
      unset($form['#info']['filter-access_id']);
    }

    // Change the label 'Important dates access' to '- Any -'.
    if ($form['#id'] == 'views-exposed-form-manage-important-dates-manage-important-dates') {
      if (isset($form['access_id']['#options']['uw_tax_important_dates_workbench_access'])) {
        $form['access_id']['#options']['uw_tax_important_dates_workbench_access'] = '- Any -';
      }
    }
  }
}
