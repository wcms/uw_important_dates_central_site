<?php

/**
 * @file
 * Theme implementation to display a important date node.
 */
?>

<h1>
  <?php
    print $title;

    if (isset($content['img_variables']) && !empty($content['img_variables'])) {
      print l(theme('image', $content['img_variables']), 'important-dates/' . $node->nid . '/impotant_dates_ical.ics', ['attributes' => ['class' => ['individual-event-ical']], 'html' => TRUE]);
    }
  ?>
</h1>

<div class="important-dates">

  <?php
    if ($content['archive']) { ?>

        <div class="view-id-uw_ct_special_alert_block form-wrapper">
          <div class="clearfix">
            <?php
              $archive_message = isset(variable_get('uw_archive_message')['value']) && variable_get('uw_archive_message')['value'] !== '' ? variable_get('uw_archive_message')['value'] : 'These important dates are provided for historical purpose.';
            ?>
            <p><?php print $archive_message; ?></p>
          </div>
        </div>
    <?php } ?>

  <h2><?php print $content['academic_year']; ?></h2>

  <div class="important-dates__description">
    <?php
    if (isset($content['description'])) {
        print $content['description'];
    }
    ?>
  </div>
  <div class="important-dates__dates">
    <?php foreach ($content['dates'] as $date) { ?>
      <div class="important-dates__dates--wrapper">
        <div class="important-dates__term">
          <?php print $date['term']; ?>
        </div>
        <div class="important-dates__date">
          <?php print $date['date']; ?>
        </div>
        <?php if($date['special_notes'] !== '') { ?>
          <div class="important-dates__special-notes">
            <?php print $date['special_notes']; ?>
          </div>
        <?php } ?>
      </div>
    <?php } ?>
  </div>

    <?php if (isset($content['tags'])) { ?>
    <div class="important-dates-tags">
        <?php if (isset($content['tags']['type'])) { ?>
        <div class="important-dates-tag important-dates-tag--type">
            <?php print render($content['tags']['type']); ?>
        </div>
        <?php } ?>
    </div>
    <?php } ?>
</div>
