<?php

/**
 * Implements RestfulEntityBaseNode class for the important dates content type.
 */
class RestfulImportantDatesRemoteCalendarResource extends RestfulEntityBaseTaxonomyTerm {

  /**
   * Overrides RestfulEntityBaseNode::publicFieldsInfo().
   */
  public function publicFieldsInfo() {
    $public_fields = parent::publicFieldsInfo();

    $public_fields['view_html'] = array(
      'callback' => array($this, 'getView'),
    );

    unset($public_fields['label']);
    unset($public_fields['self']);

    return $public_fields;
  }

  /**
   * Get entity's important date details information.
   *
   * @return string
   *   The HTML of the view.
   */
  protected function getView() {

    $query_parameters = drupal_get_query_parameters();

    // If there are sites, ensure that we send it as a
    // contextual filter properly.
    if (isset($query_parameters['sites'])) {
      $sites = implode('+', $query_parameters['sites']);
    }
    else {
      $sites = array();
    }

    if (!isset($query_parameters['navigate'])) {
      $query_parameters['navigate'] = NULL;
    }

    if (preg_match("/[0-9]{4}-[0-9]{2}-[0-9]{1,2}/ms", $query_parameters['navigate'])) {
      $display_id = 'day_remote';
    }
    else {
      $display_id = 'month_remote';
    }

    return views_embed_view('uw_view_important_dates_calendar', $display_id, $sites, $query_parameters['navigate']);
  }

}
