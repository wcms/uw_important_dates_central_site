<?php

/**
 * @file
 * Plugin important_dates_remote_calendar.
 */

$plugin = array(
  'label' => t('Important dates remote calendar'),
  'resource' => 'important_dates_remote_calendar',
  'name' => 'important_dates_remote_calendar__1_0',
  'entity_type' => 'taxonomy_term',
  'bundle' => 'uw_tax_imp_dates_remote_calendar',
  'description' => t('Export the important dates calendar view.'),
  'class' => 'RestfulImportantDatesRemoteCalendarResource',
  // Set the authentication to "cookie".
  'authentication_optional' => TRUE,
  'authentication_types' => array(
    'cookie',
  ),
);
