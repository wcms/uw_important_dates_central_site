<?php

/**
 * @file
 * Plugin important_dates_remote_list_count.
 */

$plugin = array(
  'label' => t('Important dates remote list count'),
  'resource' => 'important_dates_remote_list_count',
  'name' => 'important_dates_remote_list_count__1_0',
  'entity_type' => 'taxonomy_term',
  'bundle' => 'uw_tax_imp_dates_remote_list_count',
  'description' => t('Export the row count of list view for important dates.'),
  'class' => 'RestfulImportantDatesRemoteListCountResource',
  // Set the authentication to "cookie".
  'authentication_optional' => TRUE,
  'authentication_types' => array(
    'cookie',
  ),
);
