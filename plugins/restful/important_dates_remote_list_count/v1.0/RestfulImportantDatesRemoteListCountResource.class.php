<?php

/**
 * Implements RestfulEntityBaseNode class for "important dates" content type.
 */
class RestfulImportantDatesRemoteListCountResource extends RestfulEntityBaseTaxonomyTerm {

  /**
   * Overrides RestfulEntityBaseNode::publicFieldsInfo().
   */
  public function publicFieldsInfo() {
    $public_fields = parent::publicFieldsInfo();

    $public_fields['count'] = array(
      'callback' => array($this, 'getCount'),
    );

    $public_fields['entries'] = array(
      'callback' => array($this, 'getEntries'),
    );

    unset($public_fields['label']);
    unset($public_fields['self']);

    return $public_fields;
  }

  /**
   * Get entity's important date details information.
   *
   * @return string
   *   The number of rows in the view.
   */
  protected function getCount() {

    // Get the view that we are going to use.
    $view = views_get_view('uw_view_important_dates');

    // Set the display_id of the view.
    $view->set_display('important_dates_list_page_remote');

    // Execute the view.
    $view->pre_execute();
    $view->execute();

    // Return the number of rows.
    return $view->total_rows;
  }

  /**
   * Get entity's important date details information.
   *
   * @return array
   *   The first entry after a date.
   */
  protected function getEntries() {

    // Get the query parameters from the URL.
    $query_parameters = uw_important_dates_get_query_parameters(drupal_get_query_parameters());

    // Set the academic years.
    $academic_years = isset($query_parameters['academic_years']) ? $query_parameters['academic_years'] : NULL;

    // Set the current academic year.
    $current_academic_year = isset($query_parameters['current_academic_year']) ? $query_parameters['current_academic_year'] : NULL;

    // Get the results from the view.
    $list_view = _uw_important_dates_central_site_get_list_view_entries('uw_view_important_dates', 'important_dates_list_entries', $academic_years, $current_academic_year);

    // Return the entries from the view.
    return $list_view;
  }

}
