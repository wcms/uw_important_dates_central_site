<?php

/**
 * @file
 * Ical list remote.
 */

$plugin = array(
  'label' => t('Important dates remote ical'),
  'resource' => 'important_dates_remote_ical',
  'name' => 'important_dates_remote_ical__1_0',
  'entity_type' => 'taxonomy_term',
  'bundle' => 'uw_tax_imp_dates_remote_list_count',
  'description' => t('Export the ical view of important dates.'),
  'class' => 'RestfulImportantDatesRemoteIcalResource',
  // Set the authentication to "cookie".
  'authentication_optional' => TRUE,
  'authentication_types' => array(
    'cookie',
  ),
);
