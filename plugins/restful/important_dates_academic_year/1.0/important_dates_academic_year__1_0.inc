<?php

/**
 * @file
 * Plugin important_dates_academic_year.
 */

$plugin = array(
  'label' => t('Academic Year'),
  'resource' => 'important_dates_academic_year',
  'name' => 'important_dates_academic_year__1_0',
  'entity_type' => 'taxonomy_term',
  'bundle' => 'uw_tax_imp_dates_academic_year',
  'description' => t('Export the important dates academic year.'),
  'class' => 'RestfulImportantDatesAcademicYearResource',
);
