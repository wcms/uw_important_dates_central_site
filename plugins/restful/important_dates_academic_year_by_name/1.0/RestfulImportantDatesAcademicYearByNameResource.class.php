<?php

/**
 * Implements RestfulEntityBaseNode class for the "important dates".
 */
class RestfulImportantDatesAcademicYearByNameResource extends RestfulEntityBaseTaxonomyTerm {

  /**
   * Overrides RestfulEntityBaseNode::publicFieldsInfo().
   */
  public function publicFieldsInfo() {
    $public_fields = parent::publicFieldsInfo();

    $public_fields['academic_year'] = array(
      'callback' => array($this, 'getAcademicYear'),
    );

    unset($public_fields['label']);
    unset($public_fields['self']);

    return $public_fields;
  }

  /**
   * Get entity's important date details information.
   *
   * @return int|null
   *   The tid of the academic year.
   */
  protected function getAcademicYear() {

    // Get the parameters of the URL.
    $query_parameters = drupal_get_query_parameters();

    // If there is an academic year, process it.
    if (isset($query_parameters['academic_year'])) {

      // Break apart the academic year to get the years.
      $years = explode('-', $query_parameters['academic_year']);

      // If there is two years, continue to process.
      if (count($years) == 2) {

        // Set the academic year with the correct format.
        $academic_year_name = $years[0] . ' - ' . $years[1];

        // Get the taxonomy term using the name.
        $academic_year_terms = taxonomy_get_term_by_name($academic_year_name);

        // If there is a term, process it.
        if (isset($academic_year_terms)) {

          // Step through each term and get the tid.
          // We are really only looking for the first one.
          foreach ($academic_year_terms as $academic_year_term) {
            $academic_year = $academic_year_term->tid;
            break;
          }
        }
      }

      // If there is a academic year, return it, if not, return NULL.
      return isset($academic_year) ? $academic_year : NULL;
    }
    else {

      // If there is no academic year in the parameters, return NULL.
      return;
    }
  }

}
