<?php

/**
 * @file
 * Academic year by name.
 */

$plugin = array(
  'label' => t('Important dates academic year by name'),
  'resource' => 'important_dates_academic_year_by_name',
  'name' => 'important_dates_academic_year_by_name__1_0',
  'entity_type' => 'taxonomy_term',
  'bundle' => 'uw_tax_imp_dates_remote_list_count',
  'description' => t('Export the academic year by name.'),
  'class' => 'RestfulImportantDatesAcademicYearByNameResource',
  // Set the authentication to "cookie".
  'authentication_optional' => TRUE,
  'authentication_types' => array(
    'cookie',
  ),
);
