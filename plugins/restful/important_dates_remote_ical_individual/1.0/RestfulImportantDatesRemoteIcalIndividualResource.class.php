<?php

/**
 * Implements RestfulEntityBaseNode class for the "important dates" ical.
 */
class RestfulImportantDatesRemoteIcalIndividualResource extends RestfulEntityBaseTaxonomyTerm {

  /**
   * Overrides RestfulEntityBaseNode::publicFieldsInfo().
   */
  public function publicFieldsInfo() {
    $public_fields = parent::publicFieldsInfo();

    $public_fields['ical'] = array(
      'callback' => array($this, 'getIcal'),
    );

    unset($public_fields['label']);
    unset($public_fields['self']);

    return $public_fields;
  }

  /**
   * Get entity's important date details information.
   *
   * @return string
   *   The ical of the view.
   */
  protected function getIcal() {

    // Get the view that we are going to use.
    $view = views_get_view('uw_view_imp_dates_ical');

    // Set the display_id of the view.
    $view->set_display('imp_dates_ical_individual_remote');

    // Execute the view.
    $view->pre_execute();
    $view->execute();

    // Return the number of rows.
    return $view->render();
  }

}
