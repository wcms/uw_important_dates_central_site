<?php

/**
 * @file
 * Ical individual remote.
 */

$plugin = array(
  'label' => t('Important dates remote ical individual'),
  'resource' => 'important_dates_remote_ical_individual',
  'name' => 'important_dates_remote_ical_individual__1_0',
  'entity_type' => 'taxonomy_term',
  'bundle' => 'uw_tax_imp_dates_remote_list_count',
  'description' => t('Export the ical individual entry view of important dates.'),
  'class' => 'RestfulImportantDatesRemoteIcalIndividualResource',
  // Set the authentication to "cookie".
  'authentication_optional' => TRUE,
  'authentication_types' => array(
    'cookie',
  ),
);
