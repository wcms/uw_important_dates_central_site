<?php

/**
 * Implements RestfulEntityBaseNode class for "important dates" content type.
 */
class RestfulImportantDatesCalendarCountResource extends RestfulEntityBaseTaxonomyTerm {

  /**
   * Overrides RestfulEntityBaseNode::publicFieldsInfo().
   */
  public function publicFieldsInfo() {
    $public_fields = parent::publicFieldsInfo();

    $public_fields['count'] = array(
      'callback' => array($this, 'getCount'),
    );

    unset($public_fields['label']);
    unset($public_fields['self']);

    return $public_fields;
  }

  /**
   * Get entity's important date details information.
   *
   * @return string
   *   The number of rows in the view.
   */
  protected function getCount() {

    // Get the URL parameters.
    $query_parameters = uw_important_dates_get_query_parameters(drupal_get_query_parameters());

    // Set the filter date.
    if (isset($query_parameters['navigate'])) {
      $filter_date = $query_parameters['navigate'];
    }
    else {
      $filter_date = date('Y-m');
    }

    if (isset($query_parameters['max_date']) && isset($query_parameters['min_date']) && isset($query_parameters['operator']) && $query_parameters['operator'] == 'between') {
      $dates['max_date'] = $query_parameters['max_date'];
      $dates['min_date'] = $query_parameters['min_date'];
      $operator = $query_parameters['operator'];
    }

    return _uw_important_dates_central_site_get_calendar_count($operator, $query_parameters, $dates);
  }

}
