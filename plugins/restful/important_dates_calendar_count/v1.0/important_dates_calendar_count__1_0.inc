<?php

/**
 * @file
 * Plugin important_dates_calendar_count.
 */

$plugin = array(
  'label' => t('Important dates calendar count'),
  'resource' => 'important_dates_calendar_count',
  'name' => 'important_dates_calendar_count__1_0',
  'entity_type' => 'taxonomy_term',
  'bundle' => 'uw_tax_imp_dates_calendar_count',
  'description' => t('Get the number of entries from the calendar.'),
  'class' => 'RestfulImportantDatesCalendarCountResource',
  // Set the authentication to "cookie".
  'authentication_optional' => TRUE,
  'authentication_types' => array(
    'cookie',
  ),
);
