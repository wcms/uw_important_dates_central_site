<?php

/**
 * @file
 * Plugin important_dates.
 */

$plugin = array(
  'label' => t('Important Dates'),
  'resource' => 'important_dates',
  'name' => 'important_dates__1_0',
  'entity_type' => 'node',
  'bundle' => 'uw_ct_important_dates_central',
  'description' => t('Export the important dates content type.'),
  'class' => 'RestfulImportantDatesResource',
  // Set the authentication to "cookie".
  'authentication_optional' => TRUE,
  'authentication_types' => array(
    'cookie',
  ),
);
