<?php

/**
 * Implements RestfulEntityBaseNode class for the important dates content type.
 */
class RestfulImportantDatesResource extends RestfulEntityBaseNode {

  /**
   * Overrides RestfulEntityBaseNode::publicFieldsInfo().
   */
  public function publicFieldsInfo() {
    $public_fields = parent::publicFieldsInfo();

    // The title of the important date.
    $public_fields['title'] = array(
      'property' => 'title',
    );

    // The academic year of the important date.
    $public_fields['academic_year'] = array(
      'property' => 'field_uw_imp_dates_academic_year',
      'sub_property' => 'name',
    );

    // The academic year of the important date.
    $public_fields['academic_year_tid'] = array(
      'property' => 'field_uw_imp_dates_academic_year',
      'sub_property' => 'tid',
    );

    // The type of important date.
    $public_fields['type'] = array(
      'property' => 'field_imp_dates_type',
      'sub_property' => 'name',
    );

    // The description of the important date.
    $public_fields['description'] = array(
      'property' => 'field_uw_imp_dates_description',
      'sub_property' => 'value',
    );

    // The actual dates and academic terms with any special instructions
    // of the important date.
    $public_fields['details'] = array(
      'property' => 'nid',
      'process_callbacks' => array(
        array($this, 'getImpDateDetails'),
      ),
    );

    // The sites to push the important date to.
    $public_fields['sites'] = array(
      'property' => 'field_uw_imp_dates_sites_push',
    );

    // The audience for the important date.
    $public_fields['audience'] = array(
      'property' => 'field_uw_imp_dates_audience',
      'process_callbacks' => array(
        array($this, 'getTaxonomyNames'),
      ),
    );

    // The keywords for the important date.
    $public_fields['keywords'] = array(
      'property' => 'field_uw_tax_imp_dates_keywords',
      'process_callbacks' => array(
        array($this, 'getTaxonomyNames'),
      ),
    );

    // Unset the label, since we are going to be using title.
    unset($public_fields['label']);

    return $public_fields;
  }

  /**
   * Get entity's important date details information.
   *
   * @param int $nid
   *   The entity id.
   *
   * @return array
   *   The details of the important date.
   */
  protected function getImpDateDetails($nid) {

    // Get the entity wrapper for the node.
    $wrapper = entity_metadata_wrapper('node', $nid);

    // Get the entity wrapper of the important date details.
    $date_details_fields = $wrapper->field_uw_imp_date_details->value();

    $academic_year = uw_important_dates_central_site_parse_academic_year($wrapper->field_uw_imp_dates_academic_year->name->value());

    // Step through detail and get all the details.
    foreach ($date_details_fields as $date_details_field) {

      // Get the paragraph item for the detail.
      $date_details = entity_metadata_wrapper('paragraphs_item', $date_details_field->item_id);

      // Get the taxonomy term name for the academic term.
      $term = taxonomy_term_load($date_details->field_uw_imp_dates_academic_term->value()->tid);

      // Get the date object of the detail.
      $date = $date_details->field_uw_imp_dates_date->value();

      // Get the special notes of the detail.
      $special_notes = $date_details->field_uw_imp_dates_special_notes->value();

      // Store all the details.
      $details[] = array(
        'academic_term' => uw_important_dates_central_site_format_academic_year($academic_year, $term->name),
        'start_date' => date('Y-m-d', strtotime($date['value'])),
        'end_date' => $date['value'] !== $date['value2'] ? date('Y-m-d', strtotime($date['value2'])) : '',
        'special_notes' => $special_notes,
      );
    }

    return $details;
  }

  /**
   * Get entity's taxonomy information.
   *
   * @param array $taxonomies
   *   The taxonomy term entities.
   *
   * @return array
   *   The taxonomy names of the entity.
   */
  protected function getTaxonomyNames(array $taxonomies) {

    // If there are no taxonomies, return with NULL.
    if (empty($taxonomies)) {
      return NULL;
    }

    // Step through each taxonomy and get the term name.
    foreach ($taxonomies as $tax) {

      // Load in the term based on the tid.
      $term = taxonomy_term_load($tax->tid);

      // Store the taxonomy name.
      $taxonomy_names[] = $term->name;
    }

    return $taxonomy_names;
  }

}
