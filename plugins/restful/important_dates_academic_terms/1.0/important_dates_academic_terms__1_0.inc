<?php

/**
 * @file
 * Plugin important_dates_academic_terms.
 */

$plugin = array(
  'label' => t('Academic Terms'),
  'resource' => 'important_dates_academic_terms',
  'name' => 'important_dates_academic_terms__1_0',
  'entity_type' => 'taxonomy_term',
  'bundle' => 'uw_tax_imp_dates_academic_terms',
  'description' => t('Export the important dates academic terms.'),
  'class' => 'RestfulImportantDatesAcademicTermsResource',
);
