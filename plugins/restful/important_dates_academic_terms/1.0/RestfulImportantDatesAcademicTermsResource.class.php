<?php

/**
 * Implements RestfulEntityBaseTaxonomyTerm class for academic term taxonomy.
 */
class RestfulImportantDatesAcademicTermsResource extends RestfulEntityBaseTaxonomyTerm {

  /**
   * Overrides RestfulEntityBaseNode::publicFieldsInfo().
   */
  public function publicFieldsInfo() {
    $public_fields = parent::publicFieldsInfo();

    $public_fields['weight'] = array(
      'property' => 'weight',
      'process_callbacks' => array(
        array($this, 'getWeight'),
      ),
    );

    return $public_fields;
  }

  /**
   * Get entity's weight and set as id.
   *
   * @return int
   *   The weight of the taxonomy term.
   */
  protected function getWeight($tid) {
    $term = taxonomy_term_load($tid);
    return isset($term->weight) ? ($term->weight + 1) : 0;
  }

}
