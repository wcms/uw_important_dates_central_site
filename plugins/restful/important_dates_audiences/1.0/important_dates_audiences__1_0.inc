<?php

/**
 * @file
 * Plugin important_dates_audiences.
 */

$plugin = array(
  'label' => t('Audiences'),
  'resource' => 'important_dates_audiences',
  'name' => 'important_dates_audiences__1_0',
  'entity_type' => 'taxonomy_term',
  'bundle' => 'uw_tax_imp_dates_audience',
  'description' => t('Export the important dates audiences.'),
  'class' => 'RestfulImportantDatesAudiencesResource',
);
