<?php

/**
 * @file
 * Plugin important_dates_sites.
 */

$plugin = array(
  'label' => t('Sites'),
  'resource' => 'important_dates_sites',
  'name' => 'important_dates_sites__1_0',
  'entity_type' => 'taxonomy_term',
  'bundle' => 'uw_tax_important_dates_sites',
  'description' => t('Export the important dates sites to push to.'),
  'class' => 'RestfulImportantDatesSitesResource',
);
