<?php

/**
 * @file
 * Plugin important_dates_types.
 */

$plugin = array(
  'label' => t('Types'),
  'resource' => 'important_dates_types',
  'name' => 'important_dates_types__1_0',
  'entity_type' => 'taxonomy_term',
  'bundle' => 'uw_tax_imp_dates_type',
  'description' => t('Export the important dates types.'),
  'class' => 'RestfulImportantDatesTypesResource',
);
