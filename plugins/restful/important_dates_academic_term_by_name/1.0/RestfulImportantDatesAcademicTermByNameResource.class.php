<?php

/**
 * Implements RestfulEntityBaseNode class for the "important dates".
 */
class RestfulImportantDatesAcademicTermByNameResource extends RestfulEntityBaseTaxonomyTerm {

  /**
   * Overrides RestfulEntityBaseNode::publicFieldsInfo().
   */
  public function publicFieldsInfo() {
    $public_fields = parent::publicFieldsInfo();

    $public_fields['academic_term'] = array(
      'callback' => array($this, 'getAcademicTerm'),
    );

    unset($public_fields['label']);
    unset($public_fields['self']);

    return $public_fields;
  }

  /**
   * Get entity's important date details information.
   *
   * @return int|null
   *   The tid of the academic term.
   */
  protected function getAcademicTerm() {

    // Get the parameters of the URL.
    $query_parameters = drupal_get_query_parameters();

    // If there is an academic term, process it.
    if (isset($query_parameters['academic_term'])) {

      // Get the taxonomy term using the name.
      $academic_year_terms = taxonomy_get_term_by_name($query_parameters['academic_term']);

      // If there is a term, process it.
      if (isset($academic_year_terms)) {

        // Step through each term and get the tid.
        // We are really only looking for the first one.
        foreach ($academic_year_terms as $academic_year_term) {
          $academic_year = $academic_year_term->tid;
          break;
        }
      }

      // If there is a academic term, return it, if not, return NULL.
      return isset($academic_year) ? $academic_year : NULL;
    }
    else {

      // If there is no academic term in the parameters, return NULL.
      return;
    }
  }

}
