<?php

/**
 * @file
 * Academic term by name.
 */

$plugin = array(
  'label' => t('Important dates academic term by name'),
  'resource' => 'important_dates_academic_term_by_name',
  'name' => 'important_dates_academic_term_by_name__1_0',
  'entity_type' => 'taxonomy_term',
  'bundle' => 'uw_tax_imp_dates_remote_list_count',
  'description' => t('Export the academic term by name.'),
  'class' => 'RestfulImportantDatesAcademicTermByNameResource',
  // Set the authentication to "cookie".
  'authentication_optional' => TRUE,
  'authentication_types' => array(
    'cookie',
  ),
);
