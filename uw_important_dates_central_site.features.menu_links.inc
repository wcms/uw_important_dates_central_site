<?php

/**
 * @file
 * uw_important_dates_central_site.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function uw_important_dates_central_site_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: main-menu_important-dates:important-dates/calendar.
  $menu_links['main-menu_important-dates:important-dates/calendar'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'important-dates/calendar',
    'router_path' => 'important-dates/calendar',
    'link_title' => 'Important dates',
    'options' => array(
      'attributes' => array(
        'title' => 'A list and calendar viewing of important dates',
      ),
      'alter' => TRUE,
      'identifier' => 'main-menu_important-dates:important-dates/calendar',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );
  // Exported menu link: menu-site-management_important-dates-settings:admin/config/system/uw_important_dates_central_site_settings.
  $menu_links['menu-site-management_important-dates-settings:admin/config/system/uw_important_dates_central_site_settings'] = array(
    'menu_name' => 'menu-site-management',
    'link_path' => 'admin/config/system/uw_important_dates_central_site_settings',
    'router_path' => 'admin/config/system/uw_important_dates_central_site_settings',
    'link_title' => 'Important dates settings',
    'options' => array(
      'attributes' => array(
        'title' => 'Settings for important dates',
      ),
      'identifier' => 'menu-site-management_important-dates-settings:admin/config/system/uw_important_dates_central_site_settings',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );
  // Exported menu link: menu-site-manager-vocabularies_important-dates-workbench-access:admin/structure/taxonomy/uw_tax_important_dates_workbench_access.
  $menu_links['menu-site-manager-vocabularies_important-dates-workbench-access:admin/structure/taxonomy/uw_tax_important_dates_workbench_access'] = array(
    'menu_name' => 'menu-site-manager-vocabularies',
    'link_path' => 'admin/structure/taxonomy/uw_tax_important_dates_workbench_access',
    'router_path' => 'admin/structure/taxonomy/%',
    'link_title' => 'Important dates workbench access',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-site-manager-vocabularies_important-dates-workbench-access:admin/structure/taxonomy/uw_tax_important_dates_workbench_access',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Important dates');
  t('Important dates settings');
  t('Important dates workbench access');

  return $menu_links;
}
