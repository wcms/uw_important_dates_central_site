<?php

/**
 * @file
 * uw_important_dates_central_site.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function uw_important_dates_central_site_field_group_info() {
  $field_groups = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_uw_imp_dates_container|paragraphs_item|uw_para_imp_date_details|form';
  $field_group->group_name = 'group_uw_imp_dates_container';
  $field_group->entity_type = 'paragraphs_item';
  $field_group->bundle = 'uw_para_imp_date_details';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Details',
    'weight' => '1',
    'children' => array(
      0 => 'field_uw_imp_dates_date',
      1 => 'field_uw_imp_dates_special_notes',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Details',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'group-uw-imp-dates-container field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $field_groups['group_uw_imp_dates_container|paragraphs_item|uw_para_imp_date_details|form'] = $field_group;

  // Translatables
  // Included for use with string extractors like potx.
  t('Details');

  return $field_groups;
}
